# Mission 5 cheat sheet


This mission serves as a much bigger game, with a whole, three-tiered infrastructure. It will take users through a vast range of topics and require a variety of skills on most basic free-tier services.


  * Given: AWS key, AWS secret key
  * Variables: $id


$ `aws configure --profile solus`

Set up the credentials for use. Region is always us-east-1 and json output is recommended.

## As user solus (Tier 1)

## Route 1: EC2 proxy SSRF
Do you know how to list the EC2 ?

Did you know what is SSRF (server-side request forgery)
https://portswigger.net/web-security/ssrf

Did you configure the new profile ?

Did you try to list s3 ?

Did you configure the new profile emmyselly ?


## Route 2: Generate new credentials for the next user

List users. 
https://docs.aws.amazon.com/cli/latest/reference/iam/list-users.html

Generate a new access key for user emmyselly.
Configure emmyselly.

https://docs.aws.amazon.com/cli/latest/reference/iam/create-access-key.html

## As user emmyselly (Tier 2)


Do you know how to list the EC2 present in the same region?

Did you know how to upload ssh public key on ec2 instance ?

Did you investigate the mail server ?

Did you list the lambda function ?

Did you try to add code in lambda function ?


## The dynamo database (Tier 3)
 
Did you try to stop the dynamo handler instance ?  

Did you try to see if you have some log ?


## Route 2: Use ec2-instance-connect

Did you know how to upload ssh public key on ec2 instance ?

## Once you are logged in the dynamo handler

Did you try to scan the table ?