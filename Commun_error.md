# COMMUN ERROR

## ERROR : You must specify a region. You can also configure your region by running "aws configure".

Make sure `~/.aws/config` contain 
```bash
[default]
region=us-east-1
```
Or

```bash 
export AWS_DEFAULT_REGION=us-east-1
```

## ERROR : If you use a WSL (windows subsytem linux) check that you have authorized the wsl in the windows firwall. Otherwise you won't be able to do missions 3,4,5.